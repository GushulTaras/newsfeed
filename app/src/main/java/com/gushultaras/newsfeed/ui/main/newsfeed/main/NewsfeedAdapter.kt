package com.gushultaras.newsfeed.ui.main.newsfeed.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.databinding.NewsItemBinding

class NewsfeedAdapter(val clickListener: NewsfeedClickListener) : RecyclerView.Adapter<NewsfeedVH>() {

    var list = ArrayList<ItemNewsVM>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsfeedVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = NewsItemBinding.inflate(inflater, parent, false)
        return NewsfeedVH(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: NewsfeedVH, position: Int) {
        val vm = list[position]
        holder.binding?.item = vm
        holder.binding?.click = object : Clicker {
            override fun onItemClick(view: View) {
                clickListener.click(list[position].news)
            }
        }
    }

    fun setItems(list: ArrayList<News>) {
        val news = ArrayList<ItemNewsVM>()
        list.forEach {
            news.add(ItemNewsVM(it))
        }

        val callback = DiffUtilCallback(this.list, news)
        val result = DiffUtil.calculateDiff(callback)

        this.list.clear()
        this.list.addAll(news)
        result.dispatchUpdatesTo(this)
    }

    fun isEmpty(): Boolean {
        return list.isEmpty()
    }

}