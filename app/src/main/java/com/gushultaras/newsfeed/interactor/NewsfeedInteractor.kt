package com.gushultaras.newsfeed.interactor

import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.network.repository.ApiRepository
import io.reactivex.Single

class NewsfeedInteractor {

    fun getNews(id: String): Single<List<News>> {
        return ApiRepository.getOrderList(id)
    }

}