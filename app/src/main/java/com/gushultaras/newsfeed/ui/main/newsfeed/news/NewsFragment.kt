package com.gushultaras.newsfeed.ui.main.newsfeed.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.databinding.FragmentNewsBinding
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : Fragment() {

    companion object {
        const val NEWS_ARG = "NEWS_ARG"
        const val TAG = "NewsFragment"

        fun start(news: News?): NewsFragment {
            val fragment = NewsFragment()
            news?.let {
                val args = Bundle()
                args.putParcelable(NEWS_ARG, news)
                fragment.arguments = args
            }
            return fragment
        }
    }

    private lateinit var vm: NewsFragmentVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentNewsBinding>(inflater, R.layout.fragment_news, container, false)
        val view = binding.root
        vm = ViewModelProviders.of(this).get(NewsFragmentVM::class.java)
        binding.vm = vm
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        vm.bind(arguments?.getParcelable(NEWS_ARG))
    }

}