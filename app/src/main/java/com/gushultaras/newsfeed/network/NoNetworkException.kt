package com.gushultaras.newsfeed.network

class NoNetworkException(message: String) : RuntimeException(message)