package com.gushultaras.newsfeed.ui.main.newsfeed.main

import com.gushultaras.newsfeed.data.News

interface NewsfeedClickListener {

    fun click(news: News)

}