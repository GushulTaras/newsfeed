package com.gushultaras.newsfeed.ui.main.newsfeed.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.interactor.NewsfeedInteractor
import com.gushultaras.newsfeed.network.NoNetworkException
import com.gushultaras.newsfeed.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


class NewsfeedMainFragmentVM : ViewModel() {

    val refreshing = ObservableBoolean()
    val emptyListVisibility = ObservableBoolean()

    private val newsfeedInteractor = NewsfeedInteractor()
    private val compositeDisposable = CompositeDisposable()

    val items = MutableLiveData<ArrayList<News>>()
    val error = SingleLiveEvent<Throwable>()

    fun loadItems() {
        if (items.value == null) {
            refreshItems()
        }
    }

    fun refreshItems() {
        val id = "1wozWr5swgtdV9PLyo2b09mtjaOD6sS2I"
        refreshing.set(true)
        emptyListVisibility.set(false)
        compositeDisposable.add(newsfeedInteractor
            .getNews(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                emptyListVisibility.set(false)
                items.value = ArrayList(it)
                refreshing.set(false)
            }, {
                if (it is NoNetworkException) {
                    emptyListVisibility.set(true)
                } else {
                    error.setValue(it)
                }
                refreshing.set(false)
            })
        )
    }

    fun bindData(items: ArrayList<News>?) {
        items.let {
            this.items.value = it
        }
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}