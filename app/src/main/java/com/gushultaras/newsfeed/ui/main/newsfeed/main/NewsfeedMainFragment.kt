package com.gushultaras.newsfeed.ui.main.newsfeed.main

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.doOnLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.databinding.FragmentNewsfeedMainBinding
import com.gushultaras.newsfeed.ui.main.newsfeed.news.NewsFragment
import kotlinx.android.synthetic.main.fragment_newsfeed_main.*

class NewsfeedMainFragment : Fragment() {

    companion object {
        private const val LIST_DATA_SAVER = "LIST_DATA_SAVER"
        private const val RECYCLER_VIEW_SAVER = "RECYCLER_VIEW_SAVER"
    }

    private var vm: NewsfeedMainFragmentVM? = null
    private var adapter: NewsfeedAdapter? = null
    private var recyclerStateSaver: Parcelable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentNewsfeedMainBinding>(
            inflater,
            R.layout.fragment_newsfeed_main,
            container,
            false
        )
        val view = binding.root
        vm = ViewModelProviders.of(this).get(NewsfeedMainFragmentVM::class.java)
        binding.vm = vm
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (adapter == null) {
            adapter = NewsfeedAdapter(object : NewsfeedClickListener {
                override fun click(news: News) {
                    fragmentManager?.beginTransaction()
                        ?.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        ?.replace(R.id.rootFrame, NewsFragment.start(news), NewsFragment.TAG)
                        ?.addToBackStack(null)?.commit()
                }
            })
        }
        initRecyclerView()
        initReloadBtn()
        subscribeData()
        restoreRecyclerViewPositionState()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        vm?.let {
            outState.putParcelableArrayList(LIST_DATA_SAVER, it.items.value as ArrayList<out Parcelable>)
        }
        outState.putParcelable(RECYCLER_VIEW_SAVER, recyclerStateSaver)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var list: ArrayList<News>? = null
        savedInstanceState?.let {
            list = it.getParcelableArrayList(LIST_DATA_SAVER)
            recyclerStateSaver = it.getParcelable(RECYCLER_VIEW_SAVER)
            restoreRecyclerViewPositionState()
        }
        if (list == null) {
            vm?.loadItems()
        } else {
            vm?.bindData(list!!)
        }
    }

    private fun initReloadBtn() {
        reload.setOnClickListener { vm?.refreshItems() }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        swipeRefreshLayout.setOnRefreshListener { vm?.refreshItems() }
    }

    private fun subscribeData() {
        vm?.items?.observe(this, Observer {
            adapter?.setItems(it)
        })
        vm?.error?.observe(this, Observer {
            showErrorDialog(it.message ?: it.localizedMessage)
        })
    }

    private fun restoreRecyclerViewPositionState() {
        recyclerStateSaver?.let {
            recyclerView.doOnLayout {
                recyclerView.layoutManager?.onRestoreInstanceState(recyclerStateSaver)
            }
        }
    }

    override fun onDestroyView() {
        recyclerView.layoutManager?.let {
            recyclerStateSaver = it.onSaveInstanceState()
        }
        super.onDestroyView()
    }

    private fun showErrorDialog(message: String) {
        context?.let {
            AlertDialog.Builder(it)
                .setTitle(getString(R.string.internetError))
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), { dialog, which -> })
                .create()
                .show()
        }
    }

}