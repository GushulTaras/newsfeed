package com.gushultaras.newsfeed.network.api

import com.google.gson.GsonBuilder
import com.gushultaras.newsfeed.App
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.network.NetworkMonitor
import com.gushultaras.newsfeed.network.dto.NewsListDTO
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ApiService {

    @GET(Endpoint.DATA_LIST)
    fun getOrderList(@Query("id") id: String): Single<NewsListDTO>

    companion object ApiFactory {

        private const val BASE_URL = "https://drive.google.com/"
        private const val CONNECT_TIMEOUT = 15L
        private const val WRITE_TIMEOUT = 60L
        private const val TIMEOUT = 15L

        fun getService(): ApiService {

            val httpBuilder = OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .addInterceptor {
                    if (NetworkMonitor.isConnected()) {
                        it.proceed(it.request())
                    } else {
                        throw RuntimeException(App.context.getString(R.string.errorNoInternetConnection))
                    }
                }

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().apply { setLenient() }.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpBuilder.build())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }

}