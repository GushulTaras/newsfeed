package com.gushultaras.newsfeed.utils

import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gushultaras.newsfeed.App
import com.gushultaras.newsfeed.R

@BindingAdapter("pictureUrl")
fun ImageView.bindPicture(photoUrl: String?) {
    if (photoUrl == null) {
        setImageDrawable(ContextCompat.getDrawable(App.context, R.drawable.bg_no_picture))
    } else {
        Glide.with(context)
            .load(photoUrl)
            .apply(RequestOptions.placeholderOf(R.drawable.bg_no_picture))
            .into(this)
    }
}

@BindingAdapter("linkText")
fun TextView.linkText(link: String?) {
    if (link != null) {
        text = link
        movementMethod = LinkMovementMethod.getInstance()
        Linkify.addLinks(this, Linkify.WEB_URLS)
    } else {
        text = null
    }
}