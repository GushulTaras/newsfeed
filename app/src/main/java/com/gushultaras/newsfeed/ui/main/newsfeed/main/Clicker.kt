package com.gushultaras.newsfeed.ui.main.newsfeed.main

import android.view.View

interface Clicker {

    fun onItemClick(view: View)

}