package com.gushultaras.newsfeed.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.ui.main.newsfeed.NewsfeedVipFragment
import com.gushultaras.newsfeed.ui.main.newsfeed.RootNewsFragment
import kotlinx.android.synthetic.main.fragment_newsfeed.*

class NewsfeedFragment : Fragment() {

    private var viewPagerAdapter: ViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_newsfeed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        initViewPager()
    }

    private fun initViewPager() {
        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = viewPagerAdapter!!.count
    }

    private inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
        val list = listOf(
            RootNewsFragment(),
            NewsfeedVipFragment()
        )

        override fun getItem(position: Int): Fragment {
            return list[position]
        }

        override fun getCount() = 2

        override fun getPageTitle(position: Int): CharSequence? {
            return getString(if (position == 0) R.string.tabNewsfeedMain else R.string.tabNewsfeedVip)
        }

    }

}