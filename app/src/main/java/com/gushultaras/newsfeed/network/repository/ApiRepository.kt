package com.gushultaras.newsfeed.network.repository

import com.gushultaras.newsfeed.data.News
import com.gushultaras.newsfeed.network.api.ApiService
import com.gushultaras.newsfeed.network.dto.NewsDTO
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

object ApiRepository {

    private val apiService = ApiService.getService()

    fun getOrderList(id: String): Single<List<News>> {
        return apiService
            .getOrderList(id)
            .toObservable()
            .map { it.news ?: ArrayList<NewsDTO>() }
            .map { it.filterNotNull() }
            .flatMapIterable { it }
            .map { it.toNews() }
            .toList()
            .subscribeOn(Schedulers.io())
    }

}