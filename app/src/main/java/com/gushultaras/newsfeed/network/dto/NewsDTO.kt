package com.gushultaras.newsfeed.network.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.gushultaras.newsfeed.data.News

data class NewsDTO(

    @SerializedName("tittle")
    @Expose
    var title: String? = null,

    @SerializedName("desc")
    @Expose
    var description: String? = null,

    @SerializedName("date")
    @Expose
    var date: String? = null,

    @SerializedName("pic")
    @Expose
    var pic: String? = null,

    @SerializedName("link")
    @Expose
    var link: String? = null

) {

    fun toNews(): News {
        return News(title, description, date, pic, link)
    }

}