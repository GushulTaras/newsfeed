package com.gushultaras.newsfeed.network

import android.content.Context
import android.net.ConnectivityManager
import com.gushultaras.newsfeed.App

object NetworkMonitor {

    fun isConnected(): Boolean {
        val cm = App.context.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (cm is ConnectivityManager) {
            val activeNetwork = cm.activeNetworkInfo
            activeNetwork != null && activeNetwork.isConnected
        } else false
    }

}