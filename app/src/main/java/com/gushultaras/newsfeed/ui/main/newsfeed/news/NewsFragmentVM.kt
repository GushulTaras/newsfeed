package com.gushultaras.newsfeed.ui.main.newsfeed.news

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.gushultaras.newsfeed.data.News

class NewsFragmentVM : ViewModel() {

    val pictureUrl = ObservableField<String>()
    val date = ObservableField<String>()
    val title = ObservableField<String>()
    val description = ObservableField<String>()
    val link = ObservableField<String>()

    fun bind(news: News?) {
        news?.let {
            pictureUrl.set(it.picture)
            date.set(it.date)
            title.set(it.title)
            description.set(it.description)
            link.set(it.link)
        }
    }

}