package com.gushultaras.newsfeed.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.ui.main.NewsfeedFragment
import com.gushultaras.newsfeed.ui.main.newsfeed.news.NewsFragment
import com.gushultaras.newsfeed.ui.notifications.NotificationsFragment
import com.gushultaras.newsfeed.ui.profile.ProfileFragment
import com.gushultaras.newsfeed.ui.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var bottomAdapter: BottomAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        setupBottomNavigationBar()
    }

    private fun setupBottomNavigationBar() {
        bottomAdapter = BottomAdapter(supportFragmentManager)

        bottom_nav.setupWithViewPager(viewPager)
        viewPager.adapter = bottomAdapter
        viewPager.offscreenPageLimit = bottomAdapter!!.count
    }

    private inner class BottomAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> NewsfeedFragment()
                1 -> SearchFragment()
                2 -> NotificationsFragment()
                else -> ProfileFragment()
            }
        }

        override fun getCount() = 4
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.fragments[0].childFragmentManager.findFragmentByTag(NewsFragment.TAG)

        if (count == null) {
            super.onBackPressed()
        } else {
            supportFragmentManager.fragments[0].childFragmentManager.popBackStack()
        }
    }

}