package com.gushultaras.newsfeed.ui.main.newsfeed.main

import androidx.recyclerview.widget.DiffUtil

class DiffUtilCallback(private val oldList: List<ItemNewsVM>, private val newList: List<ItemNewsVM>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].news.title == newList[newItemPosition].news.title
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].news == newList[newItemPosition].news
    }
}