package com.gushultaras.newsfeed.ui.main.newsfeed.main

import androidx.databinding.ObservableField
import com.gushultaras.newsfeed.data.News

class ItemNewsVM(val news: News) {

    val pictureUrl = ObservableField<String>()
    val date = ObservableField<String>()
    val title = ObservableField<String>()
    val description = ObservableField<String>()

    init {
        pictureUrl.set(news.picture)
        date.set(news.date)
        title.set(news.title)
        description.set(news.description)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ItemNewsVM

        if (news != other.news) return false

        return true
    }

    override fun hashCode(): Int {
        return news.hashCode()
    }

}