package com.gushultaras.newsfeed.ui.main.newsfeed.main

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gushultaras.newsfeed.databinding.NewsItemBinding

class NewsfeedVH(view: View) : RecyclerView.ViewHolder(view) {

    var binding: NewsItemBinding? = null

    init {
        binding = DataBindingUtil.bind<NewsItemBinding>(view)
    }

}
