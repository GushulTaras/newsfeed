package com.gushultaras.newsfeed.ui.main.newsfeed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gushultaras.newsfeed.R
import com.gushultaras.newsfeed.ui.main.newsfeed.main.NewsfeedMainFragment

class RootNewsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_root_news, container, false)

        if (savedInstanceState == null) {
            fragmentManager?.beginTransaction()?.replace(R.id.rootFrame, NewsfeedMainFragment())?.commit()
        }

        return view
    }

}