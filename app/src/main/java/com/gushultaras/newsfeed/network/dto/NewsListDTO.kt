package com.gushultaras.newsfeed.network.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NewsListDTO(

    @SerializedName("News")
    @Expose
    var news: List<NewsDTO?>? = null

)